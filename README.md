# Greasemonkey Scripts

These are just some scripts that make my life a little easier on the internet.

## Installation

### Extension

[Greasemonkey](https://www.greasespot.net/) is an open source browser extension that lets the user create little scripts to run on web pages. You can also use [Tampermonkey](https://tampermonkey.net/), which is a closed source browser extension that does the same thing.

You can install Greasemonkey for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/).

### Scripts

Once Greacemonkey is installed view the raw contents of the script you want to install.

